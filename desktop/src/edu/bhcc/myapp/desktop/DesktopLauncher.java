package edu.bhcc.myapp.desktop;

import SkinExample.SkinExample;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import edu.bhcc.StageExample.StageExample;
import edu.bhcc.cookieMonster.CookieMonster;
import edu.bhcc.feb_14.CupidGame;
import edu.bhcc.feb_14.StartGame;
import edu.bhcc.feb_21.Calculator;
import edu.bhcc.feb_28.Shakespeare;
import edu.bhcc.myapp.MyApp;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = "Cookie Catch";
		//config.width = 500;
		config.width = 800;
		config.height = 800;


		new LwjglApplication(new Shakespeare(), config);
	}
}
