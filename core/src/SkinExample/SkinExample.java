package SkinExample;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import edu.bhcc.StageExample.MyInputProcessor;
import edu.bhcc.StageExample.MyTextureActor;

public class SkinExample extends ApplicationAdapter {
	private Stage stage;
	Texture texture;
	Image image;
	Color backgroundColor;
	Skin skin;


	public void create () {

		stage = new Stage(new ScreenViewport());

		backgroundColor = new Color(1, 1, 1, 1);

		skin = new Skin(Gdx.files.internal("defaultSkin/uiskin.json"));

		//Creates a class that will delegate input events to multiple processors
		InputMultiplexer multiplexer = new InputMultiplexer();
		//The State is an input processor
		multiplexer.addProcessor(stage);

		//Sets the input processor for this application
		Gdx.input.setInputProcessor(multiplexer);

		TextButton myButton = new TextButton("My Button", skin);

		Window window = new Window("My Window", skin);

		Table main = new Table();

		main.add(myButton);

		window.add(main);

		window.setMovable(true);
		window.setResizable(true);

		stage.addActor(window);

	}

	public void resize (int width, int height) {

		stage.getViewport().update(width, height, true);
	}

	public void render () {
		float delta = Gdx.graphics.getDeltaTime();

		Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);

		stage.draw();
	}

	public void dispose () {
		stage.dispose();
	}
}
