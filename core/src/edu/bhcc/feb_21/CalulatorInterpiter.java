package edu.bhcc.feb_21;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.lang.reflect.Array;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class CalulatorInterpiter {

    private String strValue = "";
    private int internalValue = 0;
    private Character command;
    private ArrayList<Float> numbers;
    private ArrayList<String> commands;


    public CalulatorInterpiter(){
        numbers = new ArrayList<>();
        commands = new ArrayList<>();
    }

    /*public String calulate(String rawCommands) {

        rawCommands = rawCommands.replaceAll("[^A-Za-z0-9|+|\\-|*]", "");

        numbers.clear();
        commands.clear();

        String[]  numbersAry = rawCommands.split("[+|\\-|*]");
        String[]  commandsAry = rawCommands.split("\\d+");


        for(String num : numbersAry){
            numbers.add(NumberUtils.toFloat(num));
        }

        for(String cmd : commandsAry){
            if(StringUtils.isNotBlank(cmd.trim())) commands.add(cmd);
        }


        if(numbers.size() <= commands.size()){
            return "Error";
        }

        for(int i = 0; i <  commands.size() ; i++){
            String cmd = commands.get(i);

            if(cmd.equals("+")){
                if(i < 1)
                    internalValue = numbers.get(i) + numbers.get(i+1);
                else
                    internalValue += numbers.get(i+1);
            }

            if(cmd.equals("-")){
                if(i < 1)
                    internalValue = numbers.get(i) - numbers.get(i+1);
                else
                    internalValue -= numbers.get(i+1);
            }

            if(cmd.equals("*")){
                if(i < 1)
                    internalValue = numbers.get(i) * numbers.get(i+1);
                else
                    internalValue *= numbers.get(i+1);
            }
        }

        return  ""+internalValue;
    }*/


    public String internalCal(String rawCommands) {

        rawCommands = rawCommands.replaceAll("[^A-Za-z0-9|+|\\-|*]", "");

        numbers.clear();
        commands.clear();

        String[]  numbersAry = rawCommands.split("[+|\\-|*]");
        String[]  commandsAry = rawCommands.split("\\d+");


        for(String num : numbersAry){
            if(StringUtils.isNotBlank(num.trim()))numbers.add(NumberUtils.toFloat(num));
        }

        for(String cmd : commandsAry){
            if(StringUtils.isNotBlank(cmd.trim())) commands.add(cmd);
        }

        if(commands.isEmpty() || numbers.size() == 1){
            return rawCommands;
        }

        if(numbers.size() <= commands.size()){
            return "Error";
        }

        int commandIndex = 0;

        if((commandIndex = commands.indexOf("*")) >= 0){
            internalValue = (int)(numbers.get(commandIndex) * numbers.get(commandIndex+1));

            String right = "";
            String left = "";

            if(commandIndex > 0){
                left = rawCommands.substring(0, rawCommands.indexOf("*")-1);
            }
            if(commandIndex < commands.size()-1){
                int rightInd = rawCommands.indexOf("*");
                right = rawCommands.substring(rightInd + 2, rawCommands.length());

                while (Character.isDigit(right.charAt(0))){
                    right = right.substring(1);
                }
            }

            return internalCal(left+internalValue+right);
        }
        if((commandIndex = commands.indexOf("+")) >= 0){
            internalValue = (int)(numbers.get(commandIndex) + numbers.get(commandIndex+1));

            String right = "";
            String left = "";

            if(commandIndex > 0){
                left = rawCommands.substring(0, rawCommands.indexOf("+")-1);
            }
            if(commandIndex < commands.size()-1){
                int rightInd = rawCommands.indexOf("+");
                right = rawCommands.substring(rightInd + 2, rawCommands.length());

                while (Character.isDigit(right.charAt(0))){
                    right = right.substring(1);
                }
            }

            return internalCal(left+internalValue+right);
        }
        if((commandIndex = commands.indexOf("-")) >= 0){
            internalValue = (int)(numbers.get(commandIndex) - numbers.get(commandIndex+1));

            String right = "";
            String left = "";


            if(commandIndex > 0){
                left = rawCommands.substring(0, rawCommands.indexOf("-")-1);
            }
            if(commandIndex < commands.size()-1){
                int rightInd = rawCommands.indexOf("-");
                right = rawCommands.substring(rightInd + 2, rawCommands.length());

                while (Character.isDigit(right.charAt(0))){
                    right = right.substring(1);
                }
            }

            return internalCal(left+internalValue+right);
        }

        return  ""+internalValue;
    }

    public int getNextOpperand(int index, String str){
        int toRet = (int)Float.MAX_VALUE;

        String[] opperands = new String[]{"+", "-", "*"};


        for(String opp: opperands){
            toRet = Math.min(toRet, str.indexOf(opp));
        }

        return toRet;
    }

    public float getInternalValue(){
        return internalValue;
    }

    public float add(float value){
        return  getInternalValue() + value;
    }

    public float sub(float value){
        return  getInternalValue() - value;
    }

    public float mul(float value){
        return  getInternalValue() * value;
    }

}
