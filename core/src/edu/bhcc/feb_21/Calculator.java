package edu.bhcc.feb_21;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.kotcrab.vis.ui.VisUI;

import java.util.Objects;

public class Calculator extends ApplicationAdapter {
        private Stage stage;
        Texture texture, dragStar, terminalStar;
        Color backgroundColor;
        Skin skin;
        CalulatorGUI calulatorGUI;


        public void create () {

            stage = new Stage(new ScreenViewport());

            backgroundColor = new Color(1, 1, 1, 1);

            skin = new Skin(Gdx.files.internal("defaultSkin/uiskin.json"));

            Gdx.input.setInputProcessor(stage);

            CalulatorInterpiter calulatorInterpiter = new CalulatorInterpiter();

            calulatorGUI = new CalulatorGUI(calulatorInterpiter, skin);

            Window calcWindow = new Window("Number Cruncher" ,skin);

            calcWindow.add(calulatorGUI);

            stage.addActor(calcWindow);

            calcWindow.setWidth(calcWindow.getPrefWidth());
            calcWindow.setHeight(calcWindow.getPrefHeight());


        }


        public void resize (int width, int height) {
            stage.getViewport().update(width, height, true);
        }

        public void render () {
            float delta = Gdx.graphics.getDeltaTime();

            Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);

            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            stage.act(delta);

            stage.draw();
        }

        public void dispose () {
            stage.dispose();
        }
    }
