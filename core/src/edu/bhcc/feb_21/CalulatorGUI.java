package edu.bhcc.feb_21;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import org.apache.commons.lang3.ArrayUtils;

import java.util.Objects;

public class CalulatorGUI extends Table {

    private TextButton btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btnPlus, btnSub, btnMul, btnEql;
    private TextField textField;
    private String commands = "";
    private CalulatorInterpiter calulatorInterpiter;
    private Skin skin;

    public CalulatorGUI(CalulatorInterpiter calulatorInterpiter, Skin skin) {
        btn0 = createButton(skin, "0", '0');
        btn1 = createButton(skin, "1", '1');
        btn2 = createButton(skin, "2", '2');
        btn3 = createButton(skin, "3", '3');
        btn4 = createButton(skin, "4", '4');
        btn5 = createButton(skin, "5", '5');
        btn6 = createButton(skin, "6", '6');
        btn7 = createButton(skin, "7", '7');
        btn8 = createButton(skin, "8", '8');
        btn9 = createButton(skin, "9", '9');
        btnPlus = createButton(skin, "+", '+');
        btnSub = createButton(skin, "-", '-');
        btnMul = createButton(skin, "x", '*');
        btnEql = new TextButton("=", skin);

        textField = new TextField("", skin);

        textField.setMessageText("Give me something to calculate");

        this.skin = skin;

        final char[]  acceptedChars =  new char[]{'x', '+', '*', '-', '.'};

        textField.setTextFieldFilter(new TextField.TextFieldFilter() {
                @Override
                public  boolean acceptChar(TextField textField, char c) {
                    if (ArrayUtils.contains(acceptedChars, c) || Character.isDigit(c)){
                        return true;
                    }
                    return false;
                }
            });

        initGUI();

        btnEql.addListener(new ChangeListener(){
            @Override
            public void changed (ChangeEvent event, Actor actor){
                String output = calulatorInterpiter.internalCal(commands);
                allClear();
                textField.setText(output);
            }
        });

        textField.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if(Objects.equals(c, "x"))
                    c = '*';
                commands += String.valueOf(c);
            }
        });
    }

    public void initGUI(){
        add(textField).colspan(4).row();
        add(btn7);
        add(btn8).fillX();
        add(btn9).fillX();
        add(btnMul).fillX().row();
        add(btn4).fillX();
        add(btn5).fillX();
        add(btn6);
        add(btnSub).fillX().row();
        add(btn1).fillX();
        add(btn2);
        add(btn3).fillX();
        add(btnPlus).fillX().row();
        add(btn0).fillX().colspan(2);
        add(btnEql).fillX().colspan(2).row();

        SelectBox<String> selectBox = new SelectBox<String>(skin);
        selectBox.setItems("item 1", "item 2", "item 3");
        add(selectBox).colspan(4).fillX();



    }

    public void allClear(){
        commands = "";
        textField.setText("");
    }

    public String getCommands(){
       return commands;
    }

    public void setDisplayedText(String text){
        this.textField.setText(text);
    }

    public TextButton createButton(Skin skin, String message, char value){
        TextButton toRet = new TextButton(message, skin);

        toRet.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);

                textField.setText(textField.getText()+message);

                commands += String.valueOf(value);
            }
        });

        return  toRet;
    }
}
