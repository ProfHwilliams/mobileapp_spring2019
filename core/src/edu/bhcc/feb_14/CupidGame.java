package edu.bhcc.feb_14;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.Objects;


/**
 * A simple game we created in class. The user the smaller of 2 stars into the larger of 2 stars. The program will snap
 * the smaller star into the larger start if the smaller star  is within  a predefined proximity.
 */
public class CupidGame extends ApplicationAdapter {
	private Stage stage;
	Texture texture, dragStar, terminalStar;
	Image cupid, arrow, heart;
	Color backgroundColor;
	Skin skin;


	public void create () {

		stage = new Stage(new ScreenViewport());

		backgroundColor = new Color(1, 1, 1, 1);

		skin = new Skin(Gdx.files.internal("defaultSkin/uiskin.json"));

		Gdx.input.setInputProcessor(stage);

		Image cupid = new Image(new Texture("cupid.png"));

		stage.addActor(cupid);

		cupid.setOrigin(Align.center);

		cupid.setBounds(200, 0, 300, 300);

		heart = new Image(new Texture("heart.jpg"));

		heart.setOrigin(Align.center);
		heart.setBounds(MathUtils.random(700), 500, 200, 200);

		stage.addActor(heart);

		cupid.addListener(new ClickListener(){
			@Override
			public void touchDragged(InputEvent event, float x, float y, int pointer) {
				super.touchDragged(event, x, y, pointer);

				cupid.moveBy(x - cupid.getWidth()*.5f, 0);
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
				if(Objects.nonNull(arrow)) return;
				arrow = new Image(new Texture("arrow.png"));
				//arrow.setOrigin(Align.center);
				arrow.setBounds(cupid.getX(), 300, 100, 100);
				arrow.rotateBy(270);

				stage.addActor(arrow);
			}
		});

	}

	public void updateArrow(){
		if(Objects.isNull(arrow)) return;

		arrow.setY(arrow.getY()+5);

		if(heart.getY() < arrow.getY() && Math.abs(heart.getX() - arrow.getX()) < 100){
			heart.remove();
			arrow.remove();
		}
	}


	public void resize (int width, int height) {

		stage.getViewport().update(width, height, true);
	}

	public void render () {
		float delta = Gdx.graphics.getDeltaTime();

		Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);

		stage.draw();

		updateArrow();
	}

	public void dispose () {
		stage.dispose();
	}
}
