package edu.bhcc.feb_14;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;


/**
 * A simple game we created in class. The user the smaller of 2 stars into the larger of 2 stars. The program will snap
 * the smaller star into the larger start if the smaller star  is within  a predefined proximity.
 */
public class StartGame extends ApplicationAdapter {
	private Stage stage;
	Texture texture, dragStar, terminalStar;
	Image image;
	Color backgroundColor;
	Skin skin;


	public void create () {

		stage = new Stage(new ScreenViewport());

		backgroundColor = new Color(1, 1, 1, 1);

		skin = new Skin(Gdx.files.internal("defaultSkin/uiskin.json"));

		Gdx.input.setInputProcessor(stage);

		Image terminalStar = star(stage, Color.CYAN, new Vector2(200, 200), new Vector2(200, 200));

		Image myStar = star(stage, new Color(MathUtils.random(), MathUtils.random(), MathUtils.random(), 1), new Vector2(MathUtils.random(0, 800-200), MathUtils.random(0, 800-200)), new Vector2(200, 200));

		myStar.scaleBy(-.2f);

		myStar.addListener(new ClickListener(){
			@Override
			public void touchDragged(InputEvent event, float x, float y, int pointer) {
				super.touchDragged(event, x, y, pointer);

				myStar.moveBy(x - myStar.getWidth()*.5f, y - myStar.getHeight()*.5f);
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);

				if(isOverlap(terminalStar, myStar, 30)){
					System.out.println("OVER LAP");
				}else{
					System.out.println("NO OVER LAP");
				}

				if(isOverlap(terminalStar, myStar, 100)){
					myStar.setX(terminalStar.getX() );
					myStar.setY(terminalStar.getY() );
				}
			}
		});

	}

	public boolean isOverlap(Actor largerStar, Actor smallerStar, int bound){

		float largerX = largerStar.getX();
		float largerY = largerStar.getY();

		float smallerX = smallerStar.getX();
		float smallerY = smallerStar.getY();

		System.out.println("LARGER "+largerX+" "+largerY);
		System.out.println("SMALLER "+smallerX+" "+smallerY);

		return  Math.abs(largerX - smallerX) < bound && Math.abs(largerY - smallerY) < bound;
	}

	public Image star(Stage stage, Color color, Vector2 location,  Vector2 size){
		Image toRet;

		Texture texture = new Texture("star.png");

		toRet = new Image(texture);

		toRet.setBounds(location.x, location.y, size.x, size.y);

		toRet.setColor(color);

		toRet.setOrigin(Align.center);

		stage.addActor(toRet);

		return  toRet;
	}


	public void resize (int width, int height) {

		stage.getViewport().update(width, height, true);
	}

	public void render () {
		float delta = Gdx.graphics.getDeltaTime();

		Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);

		stage.draw();
	}

	public void dispose () {
		stage.dispose();
	}
}
