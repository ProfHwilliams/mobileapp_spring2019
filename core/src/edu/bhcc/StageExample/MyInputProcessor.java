package edu.bhcc.StageExample;

import com.badlogic.gdx.InputProcessor;

public class MyInputProcessor implements InputProcessor {

    public boolean keyDown (int keycode) {
        System.out.println("KEY DOWN: "+keycode);

        return false;
    }

    public boolean keyUp (int keycode) {
        System.out.println("KEY UP: "+keycode);
        return false;
    }

    public boolean keyTyped (char character) {
        System.out.println("KEY TYPE: "+character);
        return false;
    }

    public boolean touchDown (int x, int y, int pointer, int button) {
        System.out.println("TOUCH DOWN x: "+x+" y: "+y+" Pointer: "+pointer+" Button: "+button);

        return false;
    }

    public boolean touchUp (int x, int y, int pointer, int button) {
        System.out.println("TOUCH UP x: "+x+" y: "+y+" Pointer: "+pointer+" Button: "+button);

        return false;
    }

    public boolean touchDragged (int x, int y, int pointer) {
        System.out.println("DRAG x: "+x+" y: "+y+" Pointer: "+pointer);

        return false;
    }

    public boolean mouseMoved (int x, int y) {
        System.out.println("MOUSE MOVE x: "+x+" y:"+y);

        return false;
    }

    public boolean scrolled (int amount) {
        System.out.println("SCROLL: "+amount);

        return false;
    }
}
