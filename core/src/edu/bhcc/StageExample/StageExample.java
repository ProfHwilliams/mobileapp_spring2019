package edu.bhcc.StageExample;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;

public class StageExample extends ApplicationAdapter {
	private Stage stage;
	Texture texture;
	Image image;
	Color backgroundColor;
	float degree = 0;

	public void create () {

		/**
		 * 	With this viewport creation, each unit in the stage corresponds to 1 pixel. This means the stage is never
		 * 	stretched
		 */
		stage = new Stage(new ScreenViewport());

		/**
		 * 	stage's size of 640x480 will be stretched to the screen size
		 */
		//stage = new Stage(new StretchViewport(800, 800));

		/**
		 *	The stage's size of 640x480 is scaled to fit the screen without changing the aspect ratio, black bars are
		 *	added on either side to take up the remaining space
		 */
		//stage = new Stage(new FillViewport(500, 500));

		/**
		 * 	The stage's size of 640x480 is first scaled to fit without changing the aspect ratio, then the stage's
		 * 	shorter dimension is increased to fill the screen. The aspect ratio is not changed and there are no black
		 * 	bars, but the stage may be longer in one direction
		 */
		//stage = new Stage(new ExtendViewport(800, 800));

		/**
		 * Here is an example of using ExtendViewport with a maximum size. However, the stage's size won't be increased
		 * beyond the maximum size of 800x480. This approach allows you to show more of the world to support many
		 * different aspect ratios without showing black bars.
		 */
		//stage = new Stage(new ExtendViewport(800, 800, 800, 800));

		backgroundColor = new Color(1, 1, 1, 1);

		texture = new Texture("stageExample/linux.png");

		image = new Image(texture);

		MyTextureActor myButton = new MyTextureActor(new Texture("stageExample/button.png"));

		//Create a class the processes input from the lower level libgdx event notifications
		MyInputProcessor inputProcessor = new MyInputProcessor();

		//Creates a class that will delegate input events to multiple processors
		InputMultiplexer multiplexer = new InputMultiplexer();

		//Your custom input processor
		//multiplexer.addProcessor(inputProcessor);

		//The State is an input processor
		multiplexer.addProcessor(stage);

		//Sets the input processor for this application
		Gdx.input.setInputProcessor(multiplexer);

		stage.addActor(image);
		stage.addActor(myButton);

		image.setBounds(stage.getWidth()*.5f - 150, 350, 300, 300);

		myButton.setBounds(stage.getWidth()*.5f - 100, 10, 150, 150);

		image.setOrigin(Align.center);


		myButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);

				backgroundColor.r = MathUtils.random();
				backgroundColor.g = MathUtils.random();
				backgroundColor.b = MathUtils.random();
				backgroundColor.a = MathUtils.random();

				//image.scaleBy(-.1f);
			}

			@Override
			public void touchDragged(InputEvent event, float x, float y, int pointer) {
				super.touchDragged(event, x, y, pointer);

				myButton.moveBy(x - myButton.getWidth()*.5f, 0);

				degree = myButton.getX() *.1f;
			}
		});

		degree = myButton.getX() *.1f;
	}

	public void resize (int width, int height) {

		stage.getViewport().update(width, height, true);
	}

	public void render () {
		float delta = Gdx.graphics.getDeltaTime();
		Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();

		image.rotateBy(degree);

		System.out.println(Gdx.graphics.getFramesPerSecond());
	}

	public void dispose () {
		stage.dispose();
	}
}
