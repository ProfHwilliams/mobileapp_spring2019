package edu.bhcc.cookieMonster;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

import java.util.logging.FileHandler;

public class DropItem  extends Rectangle {

    public static final int COOKIE_IMG_SIZE = 100;
    private Texture texture;
    private int dropSpeed;


    DropItem(FileHandle fileHandle, int viewportHeight, int viewportWidth, int height, int width){
        texture = new Texture(fileHandle);
        this.x = MathUtils.random(0 + CookieMonster.MONSTER_IMG_WIDTH *.4f, viewportWidth-width - CookieMonster.MONSTER_IMG_WIDTH * .4f);
        this.y = viewportHeight;
        this.width = width;
        this.height = height;
        dropSpeed = MathUtils.random(100, 300);
    }

    public int dropSpeed(){
        return  dropSpeed;
    }

    public final Texture getTexture(){
        return texture;
    }
}
