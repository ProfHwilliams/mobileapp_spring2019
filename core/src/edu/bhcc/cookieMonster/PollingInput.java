package edu.bhcc.cookieMonster;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class PollingInput {

    private Map<String, ArrayList<Consumer<Object>>> listenerPool;
    private Camera camera;
    private final Vector3 TEMP;
    private final String TOUCH_KEY = "TOUCH";
    private final String KEYBOARD_KEY = "KEYBOARD";

    public PollingInput(Camera camera){
        this.camera = camera;
        listenerPool = new HashMap<>();
        TEMP = new Vector3();
    }

    public void update(){

        //Is the screen being touched by a mouse or a finger?
        if(Gdx.input.isTouched() && listenerPool.containsKey(TOUCH_KEY)) {

            ArrayList<Consumer<Object>> inputConsumerList = listenerPool.get(TOUCH_KEY);

            //Convert the screen coordinates of the touch to the camera coordinates
            TEMP.setZero();
            TEMP.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(TEMP);

            inputConsumerList.forEach(c -> c.accept(TEMP));

            //DON'T DO THE CODE BELOW, the GC will not like you
            //inputConsumerList.forEach(c -> c.accept(new Vector3(TEMP)));
        }

        if(Gdx.input.isKeyPressed(Input.Keys.LEFT) && listenerPool.containsKey(TOUCH_KEY)){

            ArrayList<Consumer<Object>> inputConsumerList = listenerPool.get(KEYBOARD_KEY);

            inputConsumerList.forEach(c -> c.accept(Input.Keys.LEFT));


        }
        else if(Gdx.input.isKeyPressed(Input.Keys.RIGHT) && listenerPool.containsKey(TOUCH_KEY)){

            ArrayList<Consumer<Object>> inputConsumerList = listenerPool.get(KEYBOARD_KEY);

            inputConsumerList.forEach(c -> c.accept(Input.Keys.RIGHT));


        }
    }

    public void addIsTouchedListener(Consumer<? super Object> listener){

        ArrayList<Consumer<Object>> value = listenerPool.computeIfAbsent(TOUCH_KEY, (k) -> new ArrayList<>());

        value.add(listener);
    }

    public void addKeyboardListener(Consumer<? super Object> listener){

        ArrayList<Consumer<Object>> value = listenerPool.computeIfAbsent(KEYBOARD_KEY, (k) -> new ArrayList<>());

        value.add(listener);
    }
}
