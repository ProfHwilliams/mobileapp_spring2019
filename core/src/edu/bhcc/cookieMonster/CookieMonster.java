package edu.bhcc.cookieMonster;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.ArrayList;
import java.util.Objects;

/*
 * Loosely based off of this example here https://github.com/libgdx/libgdx/wiki/A-simple-game
 */
public class CookieMonster extends ApplicationAdapter {

    public static final int MONSTER_IMG_WIDTH = 200;
    public static final int MONSTER_IMG_HEIGHT = 194;

    public static final int MONSTER_FRAME_ADJUSTMENT = 120;

    public static final int CAMERA_WIDTH = 500;
    public static final int CAMERA_HEIGHT = 800;


    private Texture monstertImage;
    private Sound eatSound;
    private Sound noSound;
    private Music gameMusic;
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private Rectangle monsterFrame;
    private PollingInput pollingInput;
    private ArrayList<DropItem> dropItems;
    private long lastDropTime;
    private SequenceAction colorAction;
    private float score = 0, difficulty = 0;

    @Override
    public void create() {
        // load the images for the droplet and the bucket, 64x64 pixels each
        monstertImage = new Texture(Gdx.files.internal("cookieMonster/cookieMonster_OpenMouth.png"));

        // load the drop sound effect and the rain background "music"
        eatSound = Gdx.audio.newSound(Gdx.files.internal("cookieMonster/nomNom.mp3"));
        gameMusic = Gdx.audio.newMusic(Gdx.files.internal("cookieMonster/sesameStreetSong.mp3"));
        noSound = Gdx.audio.newSound(Gdx.files.internal("cookieMonster/no.mp3"));

        // start the playback of the background music immediately
        gameMusic.setLooping(true);
        gameMusic.play();
        gameMusic.setVolume(.4f);

        camera = new OrthographicCamera();
        /*
         * This will make sure the camera always shows us an area of our game world that is 800x480 units wide.
         * Units are relative you may consider them as pixels, feet or meters.
         */
        camera.setToOrtho(false, CAMERA_WIDTH, CAMERA_HEIGHT);

        //Create a object that holds a collection of GUI commands to be performed.
        batch = new SpriteBatch();

        monsterFrame = new Rectangle();
        monsterFrame.x = (CAMERA_WIDTH * .5f) - (MONSTER_IMG_WIDTH * .5f);
        monsterFrame.y = 0;
        monsterFrame.width = MONSTER_IMG_WIDTH - MONSTER_FRAME_ADJUSTMENT;
        monsterFrame.height = MONSTER_IMG_HEIGHT - MONSTER_FRAME_ADJUSTMENT;

        pollingInput = new PollingInput(camera);

        pollingInput.addIsTouchedListener((vec) -> {
            //Moves the frame to the mouse or finger touch location
            monsterFrame.x = (((Vector3) vec).x - (MONSTER_IMG_WIDTH * .5f)) + (MONSTER_FRAME_ADJUSTMENT * .5f);
        });

        pollingInput.addKeyboardListener((key) -> {

            //Moves the frame to the keyboard touch location
            if (Objects.equals(Input.Keys.LEFT, key)) {
                monsterFrame.x -= 200 * Gdx.graphics.getDeltaTime();
            }
            if (Objects.equals(Input.Keys.RIGHT, key)) {
                monsterFrame.x += 200 * Gdx.graphics.getDeltaTime();
            }
        });

        dropItems = new ArrayList<DropItem>();
    }

    @Override
    public void render() {

        //Set the color the screen will default to when cleared, this is also the background color. Blue (rgba color)
        Gdx.gl.glClearColor(227f / 255f, 236f / 255f, 252f / 255f, 1);

        //Clears the screen and replaces everything with the color above
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //We must update the camera's matrix coordinates of the camera
        camera.update();

        //Tell the batch to use camera coordinate system as specified above
        batch.setProjectionMatrix(camera.combined);

        pollingInput.update();

        //Make sure our frame stays within the screen limits
        if (monsterFrame.x - (MONSTER_FRAME_ADJUSTMENT * .5f) < 0) monsterFrame.x = (MONSTER_FRAME_ADJUSTMENT * .5f);
        if (monsterFrame.x - (MONSTER_FRAME_ADJUSTMENT * .5f) > CAMERA_WIDTH - MONSTER_IMG_WIDTH)
            monsterFrame.x = CAMERA_WIDTH - MONSTER_IMG_WIDTH + (MONSTER_FRAME_ADJUSTMENT * .5f);

        if (TimeUtils.millis() - lastDropTime > Math.round(1500 * (1-difficulty))) {
            dropItem();
        }

        dropItems.removeIf((dropItem) -> {
            if (dropItem.overlaps(monsterFrame) && dropItem.y > 50f) {
                eatSound.stop();

                if (dropItem instanceof Veggie) {
                    score -= 2;
                    noSound.play();
                } else {
                    score += 2;
                    eatSound.play();
                }

                difficulty = Math.min(1, (score) / 100f);

                System.out.println(difficulty);

                return true;
            }

            return dropItem.y + Cookie.COOKIE_IMG_SIZE < 0;
        });

        dropItems.forEach((cookie) -> {
            cookie.y -= cookie.dropSpeed() * Gdx.graphics.getDeltaTime();
        });

        batch.begin();
        batch.draw(monstertImage, monsterFrame.x - (MONSTER_FRAME_ADJUSTMENT * .5f), monsterFrame.y, MONSTER_IMG_WIDTH, MONSTER_IMG_HEIGHT);

        dropItems.forEach((cookie) -> {
            batch.draw(cookie.getTexture(), cookie.x, cookie.y, Cookie.COOKIE_IMG_SIZE, Cookie.COOKIE_IMG_SIZE);
        });

        batch.end();
    }

    @Override
    public void dispose() {
        batch.dispose();
        monstertImage.dispose();
        eatSound.dispose();
        gameMusic.dispose();
    }

    public void dropItem() {
        DropItem dropItem = MathUtils.randomBoolean(difficulty) ? new Veggie(CAMERA_HEIGHT, CAMERA_WIDTH) : new Cookie(CAMERA_HEIGHT, CAMERA_WIDTH);
        dropItems.add(dropItem);
        lastDropTime = TimeUtils.millis();
    }
}
