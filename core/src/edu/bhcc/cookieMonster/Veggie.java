package edu.bhcc.cookieMonster;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

public class Veggie extends DropItem {

    public static final int COOKIE_IMG_SIZE = 100;
    private Texture cookie;
    private static final String[] VEGGIES = {"broccoli.png", "carrot.png", "corn.png"};


    public Veggie(int viewportHeight, int viewportWidth){
        super(Gdx.files.internal("cookieMonster/"+VEGGIES[MathUtils.random(VEGGIES.length - 1)]), viewportHeight, viewportWidth, COOKIE_IMG_SIZE, COOKIE_IMG_SIZE);
    }
}
