package edu.bhcc.cookieMonster;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

public class Cookie extends DropItem {

    public static final int COOKIE_IMG_SIZE = 100;
    private Texture cookie;


    Cookie(int viewportHeight, int viewportWidth){
        super(Gdx.files.internal("cookieMonster/cookie.png"), viewportHeight, viewportWidth, COOKIE_IMG_SIZE, COOKIE_IMG_SIZE);
    }
}
