package edu.bhcc.feb_28;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.TimeUtils;

public abstract class DelayedChangeListener extends ChangeListener implements Runnable{
	
	private long changeTime = 0, currentHash = 0, savedHash = 0;
	
    private long delay = 300;
    
    private Object target;
    
    private AtomicBoolean RUN;
    
    public DelayedChangeListener(){
    	RUN = new AtomicBoolean(true);
    }
    
    public DelayedChangeListener(Object target) {
    	this();
    	
    	this.target = target;
	}
 
	
	

	@Override
	public void run() {
		while (RUN.get()){
            try {

                if(Objects.isNull(target)) continue;

                long latestHash = target.hashCode();

                if (latestHash != currentHash) {
                	
                	currentHash = latestHash;
                	
                	registerChange();
                }

                if (savedHash != latestHash && TimeUtils.millis() > changeTime) {

                    try {
                    	delayedChangeEvent();
                    	
                        savedHash = latestHash;
                        
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
	}
	
	public Object getTarget() {
		return target;
	}

	public void setTarget(Object target) {
		this.target = target;
	}

	public long getDelay() {
		return delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}
	
	public void registerChange() {
		changeTime = TimeUtils.millis() + delay;

        immedateChangeEvent();
	}
	
	public final void changed (ChangeEvent event, Actor actor) {
		registerChange();
	}
	
	/**
	 * Invoked whenever a change is resisted by the polling mechanism of this class
	 */
	public abstract void immedateChangeEvent();
	
	/**
	 * Invoked after the delay time has past without a delay
	 */
	public abstract void delayedChangeEvent();

}
