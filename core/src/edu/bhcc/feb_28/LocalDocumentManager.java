package edu.bhcc.feb_28;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class LocalDocumentManager extends AbstractConcurrentDocumentManager {

    private Preferences saveData;

    protected static final String DATA = "data";
    protected static final String PUBLIC_DOCUMENTS = "public_documents";

    public void flush(){
        String json = "";
        synchronized (data) {
            json = GSON.toJson(data);
        }
        saveData.putString(DATA, json);
        saveData.flush();
    }

    @Override
    public void update() {
        saveData = Gdx.app.getPreferences(PUBLIC_DOCUMENTS);

        String json = saveData.getString(DATA, "");

        data = new ConcurrentHashMap<>();

        if(StringUtils.isNotEmpty(json)) {
            Map<String, Map<String, String>> rawMap = GSON.fromJson(json, Map.class);

            rawMap.forEach((str, map) -> {
                data.put(UUID.fromString(str), new Document(map));
            });

        }
    }
}
