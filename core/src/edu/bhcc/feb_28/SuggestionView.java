package edu.bhcc.feb_28;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.google.gson.Gson;
import com.kotcrab.vis.ui.util.adapter.ArrayListAdapter;
import com.kotcrab.vis.ui.widget.*;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.BiConsumer;

public class SuggestionView extends ViewRoot{

    private VisSelectBox<SuggestionType> commands;
    private ArrayListAdapter<String, VisTable> adapter;
    private final Map<UUID, Document> TEMP_MAP;
    private ListView list;
    private Controller controller;

    public SuggestionView(Controller controller) {
        super("");

        this.controller = controller;

        commands = new VisSelectBox<>();

        commands.setItems(SuggestionType.values());

        ArrayList<String> docList = new ArrayList<String>();

        TEMP_MAP = new HashMap<>();

        adapter = new ArrayListAdapter<String, VisTable>(docList) {
            @Override
            protected VisTable createView(String word) {
                VisLabel label = new VisLabel(word);

                VisTable table = new VisTable();
                table.left();
                table.add(label).grow();
                return table;
            }
        };

        list = new ListView<String>(adapter);

        list.setHeader(null);

        getRoot().add(commands).top().padLeft(12).padBottom(7).row();

        getRoot().addSeparator().padTop(10).padBottom(10);

        getRoot().add(list.getMainTable()).grow();

        commands.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                refreshSuggestions(controller.getFocusedWord());
            }
        });

    }

    public void refreshSuggestions(String word) {
    	if(StringUtils.isBlank(word)) return;
    	
        HttpRequester.getSuggestion(commands.getSelected(), word, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(final Net.HttpResponse httpResponse) {

                    String result = httpResponse.getResultAsString();

                    if(StringUtils.isEmpty(result)) return;

                    Gson gson = new Gson();

                    List<Map<String, String>> webWords = gson.fromJson(result, List.class);

                    List<String> words = new ArrayList<>();

                    webWords.forEach(wordMap -> {
                        words.add(wordMap.get("word"));
                    });
                Gdx.app.postRunnable(() ->{
                    updateList(words);
                });
            }

            @Override
            public void failed(Throwable t) {
                System.err.println("ERROR "+t);
            }

            @Override
            public void cancelled() {

            }
        });
    }

    private void updateList(List<String> values) {
        adapter.clear();

        values.forEach((word) -> {
            adapter.add(word);
        });
    }
}

