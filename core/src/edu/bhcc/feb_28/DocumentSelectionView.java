package edu.bhcc.feb_28;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.util.adapter.AbstractListAdapter;
import com.kotcrab.vis.ui.util.adapter.ArrayListAdapter;
import com.kotcrab.vis.ui.widget.*;

import java.util.*;
import java.util.function.BiConsumer;

public class DocumentSelectionView extends ViewRoot implements BiConsumer<ChangEventType, Document> {

    private ListView list;
    private Cell listCell;
    private ArrayListAdapter<Document, VisTable> adapter;
    private Controller context;
    private VisTextButton currentSelection;
    private VisImageButton deleteButton, addButton;

    private final Map<UUID, Document> TEMP_MAP;

    public DocumentSelectionView(Controller controller){
        super("");

        this.context = controller;

        ArrayList<Document> docList = new ArrayList<Document>();

        TEMP_MAP = new HashMap<>();

        adapter = new ArrayListAdapter<Document, VisTable>(docList){
            @Override
            protected VisTable createView(Document item) {
                VisTextButton button = new VisTextButton(item.getTitle(), "toggle");

                button.addListener(new ClickListener(){
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        super.clicked(event, x, y);

                        controller.setWorkingDocument(item);

                        if(Objects.nonNull(currentSelection)){
                            currentSelection.setProgrammaticChangeEvents(false);
                            currentSelection.setChecked(false);
                            currentSelection.setProgrammaticChangeEvents(true);
                        }

                        currentSelection = button;
                    }
                });

                VisTable table = new VisTable();
                table.left();
                table.add(button).grow();
                return table;
            }
        };


        list = new ListView<Document>(adapter);

        list.setHeader(null);

        /*
        Use this if you woudl like to know if any part of the item in the list has been selected
        list.setItemClickListener(new ListView.ItemClickListener<Document>() {
            @Override
            public void clicked (Document item) {
                System.out.println("Clicked: " + item.getTitle());
            }
        });
        */

        VisTable table = new VisTable();

        deleteButton = context.getIconManager().createImageButton("delete", 20, 20, Color.DARK_GRAY);

        deleteButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);

                controller.delete();
            }
        });

        addButton = context.getIconManager().createImageButton("document", 20, 20, Color.DARK_GRAY);

        addButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);

                controller.reset();
            }
        });

        table.add(addButton).left().padRight(10);
        table.add(deleteButton).left();

        getRoot().add(table).left().padLeft(12).padTop(5).padBottom(5).row();

        getRoot().addSeparator().padTop(10).padBottom(10);

        listCell = getRoot().add(list.getMainTable()).grow();

        updateList();

    }

    public void refreshList(){
        updateList();
    }

    private void updateList(){
        TEMP_MAP.clear();
        adapter.clear();

        context.getAllDocuments(TEMP_MAP);

        TEMP_MAP.forEach((id, doc) -> {
            adapter.add(doc);
        });
    }

    @Override
    public void accept(ChangEventType changEventType, Document document) {
        switch (changEventType){
            case SAVE_END:
                refreshList();
        }
    }

}
