package edu.bhcc.feb_28;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class Document extends HashMap<String, String>{


    private final static String ID = "id";
    private final static String TITLE = "title";
    private final static String DOCUMENT = "document";
    private final static String CREATION_DATE = "creation_date";

    public Document(String title){
        this();

        setTitle(title);
    }


    public Document(String title, String document){
        this();

        setTitle(title);

        setDocument(document);
    }

    public Document(Map<String, String> map) {
        this();

        putAll(map);
    }

    public Document() {
        put(ID, UUID.randomUUID().toString());
        put(TITLE, "");
        put(DOCUMENT, "");
        put(CREATION_DATE, createCreationDate());
    }

    private String createCreationDate(){
        return Long.toString(System.currentTimeMillis());
    }


    public UUID getID(){

        String idStr = get(ID);

        Objects.requireNonNull(idStr, "The document does not have an ID!");

        return UUID.fromString(get(ID));
    }

    public String getTitle(){
        return getOrDefault(TITLE, "Unnamed");
    }

    public void setTitle(String newTitle){
        put(TITLE, newTitle);
    }

    public String getDocument(){
        return getOrDefault(DOCUMENT, "");
    }

    public void setDocument(String newDoc){
        put(DOCUMENT, newDoc);
    }

    public Date getCreationDate(){

        String strCreationDate = getOrDefault(CREATION_DATE, createCreationDate());

        long longCreationDate = Long.valueOf(strCreationDate);

        return new Date(longCreationDate);
    }

    public boolean isSavable(){
        return StringUtils.isNotBlank(getTitle());
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Document && Document.class.cast(o).getID().equals(getID());
    }
}
