package edu.bhcc.feb_28;

public enum SuggestionType {
    SIM_MEANING("Similar Meaning", "ml"),
    RHYME("Rhyme", "rel_rhy");

    private String humanValue;
    private String function;

    SuggestionType(String humanValue, String functionValue){
        this.humanValue = humanValue;
        this.function = functionValue;
    }

    public String getFunction() {
        return function;
    }

    @Override
    public String toString() {
        return humanValue;
    }
}
