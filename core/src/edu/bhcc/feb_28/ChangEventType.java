package edu.bhcc.feb_28;

enum ChangEventType {
    CREATE,
    DELETE,
    CHANGE,
    SAVE_START,
    SAVE_END,
    Error;
}