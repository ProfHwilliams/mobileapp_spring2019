package edu.bhcc.feb_28;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisWindow;

public class ViewRoot extends VisWindow{

    private VisTable root;

    public ViewRoot(String title) {
        super(title);

        root = new VisTable();

        add(root).grow();
    }

    public VisTable getRoot() {
        return root;
    }
}
