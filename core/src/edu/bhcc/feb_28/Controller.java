package edu.bhcc.feb_28;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;

public class Controller implements DocumentManager {

    private Document workingDocument;
    private DocumentView documentView;
    private SuggestionView suggestionView;
    private DocumentSelectionView documentSelectionView;
    private AbstractConcurrentDocumentManager documentManager;
    private IconManager iconManager;
    private DelayedChangeListener documentChangeListener;

    public Controller(AbstractConcurrentDocumentManager documentManager, IconManager iconManager) {
        this.documentManager = documentManager;

        ScheduledThreadPoolExecutor threadPool = new ScheduledThreadPoolExecutor(3);

        this.documentManager.update();

        threadPool.execute(this.documentManager);

        this.iconManager = iconManager;
        
        documentChangeListener = createDocumentChangeListener();
        
        documentChangeListener.setDelay(120);
        
        threadPool.execute(documentChangeListener);
    }
    
    private DelayedChangeListener createDocumentChangeListener() {
    	DelayedChangeListener toRet = new DelayedChangeListener() {

			@Override
			public void immedateChangeEvent() {
			}

			@Override
			public void delayedChangeEvent() {
				if(Objects.nonNull(suggestionView)) {
					suggestionView.refreshSuggestions(getFocusedWord());
				}
			}
    		
    	};
    	    	
    	return toRet;
    }
    
    public String getFocusedWord() {
    	String toRet = getHighLightedWord();
    	
    	return StringUtils.isNotBlank(toRet) ? toRet : getLastWord();
    }
    
    public String getHighLightedWord(){
        return Optional.ofNullable(documentView.getHighLighted()).orElse("");
    }

    public String getLastWord(){
        return Optional.ofNullable(documentView.getLastWord()).orElse("");
    }

    public void setDocumentView(DocumentView documentView) {
        this.documentView = documentView;
        
        this.documentView.addHighlightListener(new Consumer<String>() {
			
			@Override
			public void accept(String t) {
				suggestionView.refreshSuggestions(t);
			}
		});
    }

    public void setDocumentSelectionView(DocumentSelectionView documentSelectionView) {
        this.documentSelectionView = documentSelectionView;

        documentManager.addChangeListener(documentSelectionView);
    }

    public void setSuggestionView(SuggestionView suggestionView) {
        this.suggestionView = suggestionView;
    }

    public void setDocumentManager(AbstractConcurrentDocumentManager documentManager) {
        this.documentManager = documentManager;
    }

    public void setWorkingDocument(Document workingDocument) {
        this.workingDocument = workingDocument;

        if(Objects.nonNull(workingDocument)){
            documentView.setDocumentText(workingDocument.getDocument());
            documentView.setTitleText(workingDocument.getTitle());
            
            documentChangeListener.setTarget(workingDocument);
        }
    }

    public void reset(){
        this.workingDocument = null;

        documentView.clearFields();
    }

    public Document create(String title){
        Document document = new Document(title);

        this.workingDocument = document;

        add(document);

        documentSelectionView.refreshList();

        return document;
    }

    public void delete(){
        if(Objects.isNull(workingDocument))return;

        documentManager.delete(workingDocument.getID());
        documentView.clearFields();
        documentSelectionView.refreshList();
    }

    public Document getWorkingDocument(){
        return workingDocument;
    }

    public IconManager getIconManager() {
        return iconManager;
    }

    @Override
    public void update() {
        documentManager.update();
    }

    @Override
    public <T extends Map<UUID, Document>> void getAllDocuments(T conduit) {
        documentManager.getAllDocuments(conduit);
    }

    @Override
    public void addChangeListener(BiConsumer<ChangEventType, Document> consumer) {
        documentManager.addChangeListener(consumer);
    }

    @Override
    public Document getDocument(UUID id) {
        return documentManager.getDocument(id);
    }

    @Override
    public void add(Document document) {
        documentManager.add(document);
    }

    @Override
    public void delete(UUID id) {
        Objects.requireNonNull(id);

        documentManager.delete(id);

        documentSelectionView.refreshList();

        if(Objects.nonNull(workingDocument) && Objects.equals(workingDocument.getID(), id)){
            documentView.clearFields();
        }
    }

    @Override
    public boolean isAutoSave() {
        return documentManager.isAutoSave();
    }

    @Override
    public void terminateAutoSave() {
        documentManager.terminateAutoSave();
    }
}
