package edu.bhcc.feb_28;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.kotcrab.vis.ui.widget.VisImageButton;

import java.util.Objects;
import java.util.logging.FileHandler;

public class IconManager extends TextureAtlas {

    public IconManager (FileHandle fileHandle){
        super(fileHandle);
    }

    public VisImageButton createImageButton(String iconName, float width, float height, Color color){
        final VisImageButton out;
        final Color c = Color.LIGHT_GRAY;

        VisImageButton.VisImageButtonStyle playStyle = new VisImageButton.VisImageButtonStyle();
               
        Sprite sprite = createSprite(iconName);

        //sprite.getTexture().setFilter(Texture.TextureFilter.MipMapLinearNearest, Texture.TextureFilter.Linear);
        
        if(Objects.nonNull(color)){
            sprite.setColor(color);
        }
        else{
            sprite.setColor(Color.BLACK);
        }

        SpriteDrawable drawable = new SpriteDrawable(sprite);               

        if(Float.isFinite(height)){
            drawable.setMinHeight(height);
        }

        if(Float.isFinite(width)){
            drawable.setMinWidth(width);
        }


        playStyle.up = drawable;
        playStyle.down = drawable;
        playStyle.checked = drawable;
        playStyle.imageUp = drawable;
        playStyle.imageDown = drawable;


        out = new VisImageButton(playStyle);
        out.addListener(new ClickListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
                super.touchDown(event, x, y, pointer, button);
                if(out.isDisabled()){
                    return false;
                }
                else{
                    out.setColor(c.r, c.g, c.b, .7f);
                    return true;
                }
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                if(!out.isDisabled()){
                    out.setColor(c.r, c.g, c.b, 1f);
                }
            }

            @Override
            public void exit (InputEvent event, float x, float y, int pointer, Actor fromActor){
                super.exit(event,x,y,pointer,fromActor);
                if(!out.isDisabled()){
                    out.setColor(c.r, c.g, c.b, 1f);
                }
            }

            @Override
            public void enter (InputEvent event, float x, float y, int pointer, Actor fromActor){
                super.enter(event,x,y,pointer,fromActor);
                if(!out.isDisabled()){
                    out.setColor(c.r, c.g, c.b, .5f);
                }
            }
        });



        return out;

    }
}
