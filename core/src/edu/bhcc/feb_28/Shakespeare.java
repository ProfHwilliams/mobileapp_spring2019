package edu.bhcc.feb_28;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.kotcrab.vis.ui.VisUI;

import edu.bhcc.visColorChanger.VisColorChanger;

import org.apache.commons.lang3.RandomStringUtils;

public class Shakespeare extends ApplicationAdapter {
    private Stage stage;
    Texture texture, dragStar, terminalStar;
    Color backgroundColor;

    private Root root;


    public void create () {

        stage = new Stage(new ScreenViewport());

        backgroundColor = new Color(1, 1, 1, 1);

        Gdx.input.setInputProcessor(stage);
                
        VisUI.load(Gdx.files.internal("visui/x1/tinted.json"));

        root = new Root();

        Image img3 = new Image(new Texture("cupid.png"));

        LocalDocumentManager localDocumentManager = new LocalDocumentManager();

        IconManager iconManager = new IconManager(Gdx.files.internal("texture_packs/shakespear.atlas"));

        Controller controller = new Controller(localDocumentManager, iconManager);

        DocumentSelectionView selectionView = new DocumentSelectionView(controller);

        DocumentView documentView = new DocumentView(controller);

        SuggestionView suggestionView = new SuggestionView(controller);

        controller.setDocumentSelectionView(selectionView);

        controller.setDocumentView(documentView);

        controller.setSuggestionView(suggestionView);

        root.setLeftTable(selectionView);
        root.setMiddleTable(documentView);
        root.setRightTable(suggestionView);

        stage.addActor(root);

    }


    public void resize (int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    public void render () {
        float delta = Gdx.graphics.getDeltaTime();

        Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glTexParameteri(GL20.GL_TEXTURE_2D, GL20.GL_TEXTURE_MAG_FILTER, GL20.GL_NEAREST);

        stage.act(delta);

        stage.draw();
    }

    public void dispose () {
        stage.dispose();
    }
}
