package edu.bhcc.feb_28;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.TimeUtils;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.BiConsumer;

abstract class AbstractConcurrentDocumentManager implements DocumentManager, Runnable{

    protected Map<UUID, Document> data;
    private List<BiConsumer<ChangEventType, Document>> listenerPool;
    private boolean isAutosave = true;
    private long changeTime = 0, currentHash = 0, savedHash = 0;

    protected static final Gson GSON = new Gson();
    protected static final long DELAY = 300;

    public AbstractConcurrentDocumentManager(){
        listenerPool = new ArrayList<>();
    }

    private void testPopulated(){
        if(Objects.isNull(data))
            throw new IllegalStateException("You must call update() at least once prior to invoking this method");
    }

    public abstract void flush();

    public abstract void update();

    @Override
    public <T extends Map<UUID, Document>> void getAllDocuments(T conduit) {
        testPopulated();
        conduit.putAll(data);
    }

    private void notifyChange(ChangEventType event, Document document){
        Gdx.app.log(getClass().getSimpleName(), event.toString());

        Gdx.app.postRunnable(() ->{
            listenerPool.forEach(c -> {
                try {
                    c.accept(event, document);
                }catch (Exception e){
                    e.printStackTrace();
                }
            });
        });
    }

    @Override
    public void addChangeListener(BiConsumer<ChangEventType, Document> consumer) {
        Objects.requireNonNull(consumer);

        listenerPool.add(consumer);
    }

    @Override
    public Document getDocument(UUID id) {
        Objects.requireNonNull(id);

        testPopulated();

        return data.get(id);
    }

    @Override
    public void add(Document document) {
        Objects.requireNonNull(document);

        testPopulated();

        boolean isNew = !data.containsKey(document.getID());

        data.put(document.getID(), document);

        if (isNew)
            notifyChange(ChangEventType.CREATE, document);
    }

    @Override
    public void delete(UUID id) {
        Objects.requireNonNull(id);

        testPopulated();

        Document document = getDocument(id);

        if(Objects.nonNull(document)) {

            data.remove(id);

            notifyChange(ChangEventType.DELETE, document);
        }
    }

    @Override
    public boolean isAutoSave(){
        return isAutosave;
    }

    @Override
    public void terminateAutoSave(){
        this.isAutosave = false;
    }

    @Override
    public void run() {
        while (isAutosave){
            try {

                if(Objects.isNull(data)) continue;

                long latestHash = data.hashCode();

                if (latestHash != currentHash) {
                    changeTime = TimeUtils.millis() + DELAY;

                    currentHash = latestHash;

                    notifyChange(ChangEventType.SAVE_START, null);
                }

                if (savedHash != latestHash && TimeUtils.millis() > changeTime) {

                    try {
                        flush();
                        notifyChange(ChangEventType.SAVE_END, null);
                        savedHash = latestHash;
                    } catch (Exception e) {
                        e.printStackTrace();
                        notifyChange(ChangEventType.Error, null);
                    }
                }

                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
