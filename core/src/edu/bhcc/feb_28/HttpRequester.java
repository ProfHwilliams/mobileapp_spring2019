package edu.bhcc.feb_28;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpParametersUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class HttpRequester {

    /*new HttpResponseListener() {
        public void handleHttpResponse(HttpResponse httpResponse) {
            status = httpResponse.getResultAsString();
            //do stuff here based on response
        }

        public void failed(Throwable t) {
            status = "failed";
            //do stuff here based on the failed attempt
        }
    });*/

    private static final String ROOT_URL = "https://api.datamuse.com/words";

    private static void request(String method, String url, Map<String, String> parameters, Net.HttpResponseListener listener){

        Net.HttpRequest httpGet = new Net.HttpRequest(method);

        httpGet.setUrl(url);

        if(Objects.nonNull(parameters))
        httpGet.setContent(HttpParametersUtils.convertHttpParameters(parameters));

        Gdx.net.sendHttpRequest (httpGet, listener);
    }

    public static void getSuggestion(SuggestionType suggestionType, String word, Net.HttpResponseListener listener){

        String url = ROOT_URL+"?"+suggestionType.getFunction()+"="+word;

        request(Net.HttpMethods.GET, url, null, listener);
    }
}
