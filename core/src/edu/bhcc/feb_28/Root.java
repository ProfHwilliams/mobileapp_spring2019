package edu.bhcc.feb_28;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.MultiSplitPane;
import com.kotcrab.vis.ui.widget.VisSplitPane;
import com.kotcrab.vis.ui.widget.VisTable;

public class Root extends Table {

    private MultiSplitPane splitPane;
    private VisTable left, middle, right;

    public Root() {
       super();

       setFillParent(true);

        splitPane = new MultiSplitPane(false);

        left = new VisTable();
        middle = new VisTable();
        right = new VisTable();

        splitPane.setWidgets(left, middle, right);

        add(splitPane).grow();
    }

    public void setRightTable(Actor actor){
        right.reset();

        right.add(actor).grow();
    }

    public void setLeftTable(Actor actor){
        left.reset();

        left.add(actor).grow();
    }

    public void setMiddleTable(Actor actor){
        middle.reset();

        middle.add(actor).grow();
    }



}
