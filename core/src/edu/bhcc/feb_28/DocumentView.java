package edu.bhcc.feb_28;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.kotcrab.vis.ui.widget.*;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

public class DocumentView extends ViewRoot {


    private VisLabel titleLabelInternal;
    private VisTextField titleField;
    private ScrollableTextArea documentArea;
    private Document document;
    private Controller controller;
    private List<Consumer<String>> highlightListenerPool;

    private final String[] EMPTY_MESSAGES = {"A professional writer is an amateur who didn’t quit.", "If the book is true, it will find an audience that is meant to read it.", "Start writing, no matter what. The water does not flow until the faucet is turned on.", "You can always edit a bad page. You can’t edit a blank page.", "The first draft is just you telling yourself the story.", "There is no greater agony than bearing an untold story inside you."};

    public DocumentView(Controller controller){
        super("");

        this.controller = controller;

        titleLabelInternal = new VisLabel("Title:");
        
        highlightListenerPool = new ArrayList<>();

        createTitleTextField();

        createTextArea();

        VisTable titleTable = new VisTable();

        getRoot().add(titleLabelInternal);
        getRoot().add(titleField).fillX().row();

        getRoot().addSeparator().colspan(2).padTop(10).padBottom(10);

        getRoot().add(documentArea).colspan(2).grow();

    }

    public void clearFields(){
        boolean oldValue = titleField.getProgrammaticChangeEvents();

        titleField.setProgrammaticChangeEvents(false);
        titleField.setText("");
        titleField.setProgrammaticChangeEvents(oldValue);

        oldValue = documentArea.getProgrammaticChangeEvents();
        documentArea.setProgrammaticChangeEvents(false);
        documentArea.setText("");
        documentArea.setProgrammaticChangeEvents(oldValue);
    }

    public void setDocumentText(String newText){
        documentArea.setText(newText);
    }

    public void setTitleText(String newText){
        titleField.setText(newText);
        titleField.setInputValid(isTitleValid());
        documentArea.setDisabled(!isTitleValid());
    }

    private VisTextField createTitleTextField(){
        titleField = new VisTextField();
        titleField.setMessageText("Specify a Title");

        titleField.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                textField.setInputValid(isTitleValid());

                documentArea.setDisabled(!isTitleValid());

                Document document = controller.getWorkingDocument();

                if(Objects.isNull(document)) return;

                document.setTitle(isTitleValid() ? textField.getText() : "Unnamed");
            }
        });

        titleField.addListener(new FocusListener() {
            @Override
            public void keyboardFocusChanged(FocusEvent event, Actor actor, boolean focused) {
                super.keyboardFocusChanged(event, actor, focused);

                if(!focused) {
                    titleField.setInputValid(isTitleValid());
                }
            }
        });
        
        titleField.addListener(new ClickListener(0) {
        	@Override
        	public void clicked(InputEvent event, float x, float y) {
        		super.clicked(event, x, y);
        		
        		if(titleField.isTextSelected() && StringUtils.isNotBlank(titleField.getSelection())) {
        			notifyHighlightListeners(titleField.getSelection());
        		}
        	}
        });

        return titleField;
    }
    
    private void notifyHighlightListeners(String word) {
    	word = word.trim();
    	
    	final String formatedWord = getLastWord(word);
    	
    	highlightListenerPool.forEach(listener -> {
    		listener.accept(formatedWord);
    	});
    }
    
    public void addHighlightListener(Consumer<String> listener) {
    	highlightListenerPool.add(listener);
    }

    public String getHighLighted(){

        String selection = documentArea.getSelection();

        if(StringUtils.isBlank(selection)){
            selection = titleField.getSelection();
        }

        if(StringUtils.isBlank(selection)){
            selection = "";
        }

        return selection;
    }
    
    private String getLastWord(String words) {
    	String[] splits = words.split(" ");
        return splits.length > 0 ? splits[splits.length-1] : "";
    }

    public String getLastWord(){
        return getLastWord(documentArea.getText());
    }

    private ScrollableTextArea createTextArea(){
        documentArea = new ScrollableTextArea("");

        documentArea.setMessageText(EMPTY_MESSAGES[MathUtils.random(EMPTY_MESSAGES.length-1)]);

        documentArea.addListener(new FocusListener() {
            @Override
            public void keyboardFocusChanged(FocusEvent event, Actor actor, boolean focused) {
                super.keyboardFocusChanged(event, actor, focused);

                if(focused && !isTitleValid()) {
                    documentArea.setDisabled(true);

                    titleField.setInputValid(false);

                    documentArea.getStage().setKeyboardFocus(titleField);
                }
            }
        });

        documentArea.setTextFieldListener(new VisTextField.TextFieldListener() {
            @Override
            public void keyTyped(VisTextField textField, char c) {
                Document document = controller.getWorkingDocument();

               if (Objects.isNull(document)){
                   document = controller.create(titleField.getText());
               }

                document.setDocument(textField.getText());
            }
        });
        
        documentArea.addListener(new ClickListener(0) {
        	@Override
        	public void clicked(InputEvent event, float x, float y) {
        		super.clicked(event, x, y);
        		
        		if(documentArea.isTextSelected() && StringUtils.isNotBlank(documentArea.getSelection())) {
        			System.out.println(documentArea.getSelection());
        			notifyHighlightListeners(documentArea.getSelection());
        		}
        	}
        });

        return  documentArea;
    }

    private boolean isTitleValid(){
        return StringUtils.isNotBlank(titleField.getText());
    }

}
