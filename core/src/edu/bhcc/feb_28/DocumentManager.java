package edu.bhcc.feb_28;

import java.util.Map;
import java.util.UUID;
import java.util.function.BiConsumer;

public interface DocumentManager {

    public void update();

    public <T extends Map<UUID, Document>> void getAllDocuments(T conduit);

    public void addChangeListener(BiConsumer<ChangEventType, Document> consumer);

    public Document getDocument(UUID id);

    public void add(Document document);

    public void delete(UUID id);

    public boolean isAutoSave();

    public void terminateAutoSave();
}
