package edu.bhcc.visColorChanger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Json;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class VisColorChanger {
	
	private Map<String, Object> colorSchema;
	private Gson json;
	private final String PATH;
	
	public static final String TINT_MAP = "com.badlogic.gdx.graphics.Color";
	public static final String FONT_COLOR = "t-white";
	public static final String WIDGET_HIGHLIGHT_COLOR = "t-highlight";
	public static final String SECONDARY_WIDGET_HIGHLIGHT_COLOR = "t-highlight-medium";
	public static final String TEXT_SELECTION_COLOR = "t-highlight-dark";
	public static final String WARNING_COLOR = "t-warn";
	public static final String HOVER_AND_TEXT_DISABLE_COLOR = "t-light";
	public static final String DEFAULT_BUTTON_COLOR = "t-medium";
	public static final String WIDGET_DISABLED_COLOR = "t-medium-dark";
	public static final String BACKGROUND_COLOR = "t-dark";
	public static final String DIALOG_BACKGROUND_COLOR = "t-alpha";

	
	public VisColorChanger(String path) throws IOException {		
		json = new GsonBuilder().setPrettyPrinting().create();
		
		PATH = path;
		
		String rawFile = Utils.readTextFile(PATH);
		
		colorSchema = json.fromJson(rawFile, Map.class);
		
	}
	
	public void setColor(String key, String colorStr) throws IOException {
		setColor(key, colorStr, 1);
	}
	
	public void setColor(String key, String colorStr, float a) throws IOException {
		setColor(key, Integer.valueOf(colorStr.substring( 1, 3 ), 16 ), Integer.valueOf( colorStr.substring( 3, 5 ), 16 ), Integer.valueOf( colorStr.substring( 5, 7 ), 16 ), a);
	}
	
	
	public void setColor(String key, int r, int g, int b, float a) throws IOException {
		setColor(key, new Color(((float)r)/255f, ((float)g)/255f, ((float)b)/255f, a));
	}
	
	public void setColor(String key, Color color) throws IOException {
		Map<String, Object> tintMap = (Map<String, Object>) colorSchema.get(TINT_MAP);
		
		Map<String, Double> colorMap = (Map<String, Double>) tintMap.get(key);
		
		colorMap.put("r", (double) color.r);
		colorMap.put("g", (double) color.g);
		colorMap.put("b", (double) color.b);
		colorMap.put("a", (double) color.a);
		
	}
	
	public void flush() throws IOException {
		String  jsonRaw = json.toJson(colorSchema);
		
		Utils.writeTextFile(PATH, jsonRaw);
	}

}
