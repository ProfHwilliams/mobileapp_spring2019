package edu.bhcc.visColorChanger;

import java.io.IOException;

import org.junit.Test;

import com.badlogic.gdx.graphics.Color;

public class RunColorChange {
	
	@Test
	public void changeColor() throws IOException {
		
		String path = "/Users/HWilliams/Documents/Professor/BHCC/MobileAppDev_Spring_2019/Repo/android/assets/visui/x1/tinted.json";
		
		VisColorChanger changer = new VisColorChanger(path);
		
		//FONT_COLOR
		//WIDGET_HIGHLIGHT_COLOR
		//SECONDARY_WIDGET_HIGHLIGHT_COLOR
		//TEXT_SELECTION_COLOR
		//WARNING_COLOR
		//HOVER_AND_TEXT_DISABLE_COLOR
		//DEFAULT_BUTTON_COLOR
		//WIDGET_DISABLED_COLOR
		//BACKGROUND_COLOR
		//DIALOG_BACKGROUND_COLOR
		
		changer.setColor(VisColorChanger.BACKGROUND_COLOR, "#f4f4f4");
		changer.setColor(VisColorChanger.DIALOG_BACKGROUND_COLOR, "#b2b2b2");
		changer.setColor(VisColorChanger.DEFAULT_BUTTON_COLOR, "#ffffff");
		changer.setColor(VisColorChanger.WIDGET_HIGHLIGHT_COLOR, "#0070f9");
		changer.setColor(VisColorChanger.SECONDARY_WIDGET_HIGHLIGHT_COLOR, "#015ac6");
		changer.setColor(VisColorChanger.HOVER_AND_TEXT_DISABLE_COLOR, "#5398ed");
		changer.setColor(VisColorChanger.FONT_COLOR, "#0934680");
		changer.setColor(VisColorChanger.WIDGET_DISABLED_COLOR, "#7e93ad");
		changer.setColor(VisColorChanger.WARNING_COLOR, "#ff0000");
		
		changer.flush();
	}

}
