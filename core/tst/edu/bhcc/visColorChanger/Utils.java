package edu.bhcc.visColorChanger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.badlogic.gdx.files.FileHandle;

public class Utils {
	
	public static String readTextFile(String path) throws IOException {
		File file = new File(path);
		return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
	}
	
	public static void writeTextFile(String path, String newContent) throws IOException {
		File file = new File(path);
	
		FileUtils.write(file, newContent, StandardCharsets.UTF_8, false);
	}
	
	public static InputStream getInternalFile(String path) throws FileNotFoundException {
		File file = new File(path);
		
		if(!file.exists())
			return FileHandle.class.getResourceAsStream("/" + file.getPath().replace('\\', '/'));
		else 
			return new FileInputStream(file);
	}
	
	public static String getInternalTextFile(String path) throws IOException {
		InputStream inputStream = getInternalFile(path);
		
		return IOUtils.toString(inputStream, StandardCharsets.UTF_8); 
	}
}

