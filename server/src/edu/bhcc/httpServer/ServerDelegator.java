package edu.bhcc.httpServer;

import java.util.List;

import org.apache.http.NameValuePair;

public interface ServerDelegator {	
	public String put(List<NameValuePair> params, String body);

	public String get(List<NameValuePair> params, String body);
	
	public String post(List<NameValuePair> params, String body);
	
	public String connect(List<NameValuePair> params, String body);
	
	public String delete(List<NameValuePair> params, String body);
}
