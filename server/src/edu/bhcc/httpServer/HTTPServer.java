package edu.bhcc.httpServer;

import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import com.google.api.client.util.Data;

import rawhttp.core.RawHttp;
import rawhttp.core.RawHttpRequest;
import rawhttp.core.RawHttpResponse;
import rawhttp.core.body.BodyReader;
import rawhttp.core.server.Router;
import rawhttp.core.server.TcpRawHttpServer;

public class HTTPServer implements Router{

	private String SUCCESS = "HTTP/1.1 200 OK";
	private String SUCCESS_NO_RETURN = "HTTP/1.1 204 No Content";
	private String BAD_REQUEST = "HTTP/1.1 400 Bad Request";
	private String NOT_IMPLEMENTED = "HTTP/1.1 501 Not Implemented";
	private String SERVER_ERROR = "HTTP/1.1 500 Internal Server Error";

	private RawHttp http;
	public final int PORT;

	private final static String NEW_LINE = "\r\n";
	private String serverName = "Mobile App Example Server";
	
	private ServerDelegator delegator;


	public HTTPServer(int port, ServerDelegator delegator) {
		PORT = port;
		
		Objects.requireNonNull(delegator);
		
		this.delegator = delegator;

		http = new RawHttp();

		TcpRawHttpServer server = new TcpRawHttpServer(PORT);

		server.start(this);
		
		System.out.println("Server started: "+getEndPoint());
	}
	
	public String getEndPoint() {
		try {
			return InetAddress.getLocalHost().getHostAddress()+":"+PORT;
		} catch (UnknownHostException e) {
			return "UNKNOWN";
		}
	}

	@Override
	public Optional<RawHttpResponse<?>> route(RawHttpRequest request) {
		String method = request.getMethod();

		Methods methodEnum = Methods.valueOf(method);

		String body = "";
		String status = "";
		boolean isHealthy = true;
		List<NameValuePair> params = new ArrayList<>();

		try {

			try {
				URI uri = request.getUri();

				params = URLEncodedUtils.parse(uri, "UTF-8");
				
			} catch (Exception e) {
				isHealthy = false;
				
				status = BAD_REQUEST;

				body = "Malformed URI";

				System.err.println("Client Error "+e.getMessage());

				e.printStackTrace();
			}
			
			try {
				Optional<? extends BodyReader> bodyOp = request.getBody();
				
				if(bodyOp.isPresent()) {
					body = bodyOp.get().decodeBodyToString(Charset.forName("UTF-8"));
				}
				
			} catch (Exception e) {
				isHealthy = false;
				
				status = BAD_REQUEST;

				body = "Body Parsing Error";

				System.err.println("Client Error "+e.getMessage());

				e.printStackTrace();
			}

			if(isHealthy) {

				switch (methodEnum) {
				case CONNECT:
					body = delegator.connect(params, body);
					break;
				case DELETE:
					body = delegator.delete(params, body);
					break;
				case GET:
					body = delegator.get(params, body);
					break;
				case POST:
					body = delegator.post(params, body);
					break;
				case PUT:
					body = delegator.put(params, body);
					break;
				default:
					body = "Method is not implemented";
					status = NOT_IMPLEMENTED;
					isHealthy = false;
				}
			}
		} catch (Exception e) {
			isHealthy = false;
			status = SERVER_ERROR; 

			body = e.getClass().getSimpleName();

			System.err.println("Server Error "+e.getMessage());

			e.printStackTrace();
		}

		if(isHealthy)
			status = StringUtils.isEmpty(body) ? SUCCESS_NO_RETURN : SUCCESS;

		RawHttpResponse<?> response = http.parseResponse(status+NEW_LINE+
	    	    "Content-Type: application/json"+NEW_LINE+
	    	    "Content-Length: " + body.length()+NEW_LINE+
	    	    "Server: "+serverName+NEW_LINE+
	    	    "Date: " + new Data() +NEW_LINE+
	    	    NEW_LINE +
	    	    body);
		

		return Optional.of(response);
	}
	
	public ServerDelegator getDelegator() {
		return delegator;
	}


	public void setDelegator(ServerDelegator delegator) {
		Objects.requireNonNull(delegator);
		
		this.delegator = delegator;
	}

}
