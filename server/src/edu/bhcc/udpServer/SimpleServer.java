package edu.bhcc.udpServer;

import java.net.SocketException;

public class SimpleServer extends AbstractUDPArbiter<UDPBlob> {
	
	public static void main(String[] args) throws Exception {
		new SimpleServer(1234);
	}

	public SimpleServer(int listeningPort) throws SocketException {
		super(listeningPort);
	}

	@Override
	public void receive(UDPBlob blob) {
		System.out.println("From Client: "+blob);
		
		if(blob instanceof StringMessage) {
			StringMessage message = (StringMessage)blob;
			
			String reverse = new StringBuilder(message.getMessage()).reverse().toString();
			
			message.setMessage(reverse);
			
			send(message);
		}
	}

}
