package edu.bhcc.udpServer;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class MouseCommand extends UDPBlob{
	
	private static final long serialVersionUID = 9103781227106799532L;
	
	private float x, y; //4 bytes of info per float
	private short pointer; //2 bytes of info per short
	private boolean isPressed; //1 bit of info per boolean 

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public short getPointer() {
		return pointer;
	}

	public void setPointer(short pointer) {
		this.pointer = pointer;
	}

	public boolean isPressed() {
		return isPressed;
	}

	public void setPressed(boolean isPressed) {
		this.isPressed = isPressed;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
