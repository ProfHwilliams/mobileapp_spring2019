package edu.bhcc.udpServer;

public class TerminateCommand extends UDPBlob {

	private static final long serialVersionUID = 2199838807176688619L;
		
	@Override
	public String toString() {
		return "TERMINATE";
	}

}
