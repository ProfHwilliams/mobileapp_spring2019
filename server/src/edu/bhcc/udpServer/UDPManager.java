package edu.bhcc.udpServer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Objects;

public class UDPManager<T extends UDPBlob> {

	private InetAddress TO_IP;
	private int LISTENING_PORT, TO_PORT;
	private final DatagramSocket SOCKET;
	
	public static final int MAX_BUFFER_SIZE = 1024;
	
	public UDPManager(int listeningPort) throws SocketException {
		if(listeningPort < 0)
			SOCKET = new DatagramSocket(); 
		else
			SOCKET = new DatagramSocket(listeningPort); 
		
		LISTENING_PORT = SOCKET.getLocalPort();
	}

	public UDPManager(InetAddress toAddress, int toPort, int listeningPort) throws SocketException {
		this(listeningPort);
		
		TO_IP = toAddress;
		
		TO_PORT = toPort;
		
	}
	
	public String getEndPoint() {
		try {
			return InetAddress.getLocalHost().getHostAddress()+":"+LISTENING_PORT;
		} catch (UnknownHostException e) {
			return "UNKNOWN";
		}
	}

	private DatagramPacket createPacket(UDPBlob blob) {

		byte[] buffer = blob.toBytesTested();

		DatagramPacket packet = new DatagramPacket(buffer, buffer.length); 

		return packet;
	}

	public synchronized void send(UDPBlob blob) throws IOException {
		if(Objects.isNull(TO_IP)) {
			throw new IllegalStateException("This manager was cunstructed as a server not a client call send(UDPBlob, InetAddress, int) instead");
		}
		
		send(blob, TO_IP, TO_PORT);
	}
	
	public void send(UDPBlob blob, InetAddress toAddress, int toPort) throws IOException {

		DatagramPacket packet = createPacket(blob);
		
		packet.setAddress(toAddress);
		
		packet.setPort(toPort);

		SOCKET.send(packet);
	}

	public T receive() throws IOException {
		
		byte[] receive = new byte[MAX_BUFFER_SIZE]; 

		DatagramPacket packet = new DatagramPacket(receive, receive.length); 
		
		//blocking until data is received. once received it puts it into the packet 
		SOCKET.receive(packet); 

		//Convert the message from bytes to Java class
		T blob = UDPBlob.fromBytes(receive);
		
		blob.setFromAddress(packet.getAddress());
		
		blob.setFromPort(packet.getPort());
		
		return blob;
	}
	
	public void terminate() {
		SOCKET.close();
	}

}
