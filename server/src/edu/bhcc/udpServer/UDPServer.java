package edu.bhcc.udpServer;
import java.net.DatagramPacket; 
import java.net.DatagramSocket; 
import java.net.SocketException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean; 

public abstract class UDPServer <T extends UDPBlob> implements Runnable{ 

	private final int MAX_BUFFER_SIZE;
	private final DatagramSocket SOCKET;
	private final AtomicBoolean RUN = new AtomicBoolean(true);

	public UDPServer(int maxBufferSize, int port) throws SocketException {
		// Step 1 : Create a socket to listen at the specified port
		SOCKET = new DatagramSocket(port);
		
		MAX_BUFFER_SIZE = maxBufferSize;
	}

	@Override
	public void run() {
		System.out.println("SERVER RUNNING");

		//The actual container that info of the message is put into
		byte[] receive = new byte[MAX_BUFFER_SIZE]; 

		DatagramPacket packet = null;
		
		while (RUN.get()) { 
			try {

				// Step 2 : create a DatgramPacket to receive the data. Think of this like a package in the mail 
				packet = new DatagramPacket(receive, receive.length); 

				// Step 3 : blocking until data is received. once received it puts it into the packet 
				SOCKET.receive(packet); 

				//Convert the message from bytes to Java class
				T blob = UDPBlob.fromBytes(receive);

				if(Objects.nonNull(blob)) {
					recieved(blob);
				}

				// Exit the server if the client sends "bye" 
				if (blob instanceof TerminateCommand){ 
					terminate();
				} 

				// Clear the buffer after every message. 
				receive = new byte[MAX_BUFFER_SIZE]; 

			} catch (Exception e) {
				System.out.println("Server Exception: "+e.getMessage()); 
				e.printStackTrace();
			}
		} 
		
		try {
			terminateHook();
		} finally {
			SOCKET.close();
		}
		
	}
	
	public boolean isTerminated() {
		return RUN.get();
	}
	
	public void terminate() {
		RUN.set(false);
	}

	private void send(T blob, DatagramPacket packet) {
		//SOCKET.send(null);
	}

	abstract void recieved(T blob);

	abstract void terminateHook();

} 
