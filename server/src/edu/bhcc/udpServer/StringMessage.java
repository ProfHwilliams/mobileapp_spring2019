package edu.bhcc.udpServer;

public class StringMessage extends UDPBlob {

	private static final long serialVersionUID = 8739301738894801014L;
	private String message;
	
	
	
	public StringMessage(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return message;
	}
}
