package edu.bhcc.udpServer;
import java.io.IOException; 
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A low level class the arbitrates the UDP network communication
 * @author HWilliams
 *
 * @param <T>
 */
public abstract class AbstractUDPArbiter<T extends UDPBlob>{ 

	private final UDPManager<T> serverManager;
	private final ConcurrentLinkedQueue<T> commandQueue;
	private final AtomicBoolean RUN = new AtomicBoolean(true);
	private final Thread SENDER, RECEIVER;
	private final boolean IS_SERVER;
	
	/**
	 * Constructs a server the listens on the specified port 
	 * @param listeningPort
	 * @throws SocketException
	 */
	public AbstractUDPArbiter(int listeningPort) throws SocketException {
		this(true, null, 0, listeningPort);
	}
	
	/**
	 * Constructs a client communicates with a server at the specified IP and Port 
	 * @param listeningPort
	 * @throws SocketException
	 */
	public AbstractUDPArbiter(InetAddress serverAddress, int serverPort) throws SocketException {
		this(false, serverAddress, serverPort, -1);
	}

	private AbstractUDPArbiter(Boolean isServer, InetAddress serverAddress, int serverPort, int listeningPort) throws SocketException {
		serverManager = new UDPManager<>(serverAddress, serverPort, listeningPort);
		
		IS_SERVER = isServer;

		commandQueue  = new ConcurrentLinkedQueue<>();	

		Runnable sendRunner = () -> {
			while(RUN.get()) {

				T nextCommand = commandQueue.poll();

				if(Objects.nonNull(nextCommand)) {
					try {
						if(IS_SERVER) {
							
							Objects.requireNonNull(nextCommand.getAddress(), "The command has no return IP");
							
							serverManager.send(nextCommand, nextCommand.getAddress(), nextCommand.getPort());
							
						}else {
							serverManager.send(nextCommand);
						}
					} catch (Exception e) {
						System.out.println("Command Send Error: "+e.getMessage());
						e.printStackTrace();
					}
				}
			}
		};

		Runnable recieveRunner = () -> {
			while(RUN.get()) {
				try {
					T blob = serverManager.receive();

					if(Objects.nonNull(blob)) {
						receive(blob);
					}

				} catch (Exception e) {
					System.out.println("Command Receive Error: "+e.getMessage());
					e.printStackTrace();
				}
			}
		};

		SENDER = new Thread(sendRunner);
		SENDER.start();

		RECEIVER = new Thread(recieveRunner);
		RECEIVER.start();
		
		System.out.println("Started Server: "+getEndPoint());
	}
	
	public String getEndPoint() {
		return serverManager.getEndPoint();
	}

	/**
	 * Add the specified command to the command queue
	 * @param command
	 */
	public void send(T command) {
		commandQueue.add(command);
	}

	/**
	 * Invoked when new info is received
	 * @param blob
	 */
	public abstract void receive(T blob);

	protected UDPManager<T> getManager(){
		return serverManager;
	}

	public boolean isTerminated() {
		return RUN.get();
	}

	public void terminate() {
		RUN.set(false);
		
		serverManager.terminate();
		
		try {
			SENDER.join(1000);
		} catch (InterruptedException e) {
			System.out.println("Command Termination Error: "+e.getMessage());
			e.printStackTrace();
		}
		try {
			RECEIVER.join(1000);
		} catch (InterruptedException e) {
			System.out.println("Command Termination Error: "+e.getMessage());
			e.printStackTrace();
		}
	}
} 
