package edu.bhcc.udpServer;

import java.io.Serializable;
import java.net.InetAddress;
import java.nio.BufferOverflowException;

import org.apache.commons.lang3.SerializationUtils;

/**
 * A low level class for standardize communication via UDP connection
 * @author HWilliams
 *
 */
public class UDPBlob implements Serializable{
	
	private static final long serialVersionUID = -6972901625870926738L;
	private transient InetAddress fromAddress;
	private transient int fromPort;


	/**
	 * Constructs a instance of this object
	 */
	public UDPBlob() {
	}
	
	/**
	 * Tests if this object when converted to a byte array is larger then the specified buffer limit. If 
	 * it is larger than the specified limit, then it will throw a BufferOverflowException
	 * @return The byte array that corresponds to this instance of the object
	 */
	public byte[] toBytesTested() throws BufferOverflowException{
		byte[] toRet = this.toBytes();
		
		if(toRet.length > UDPManager.MAX_BUFFER_SIZE) {
			throw new BufferOverflowException();
		}
		
		return toRet;
	}

	/**
	 * Converts this object to a byte array.
	 * @return The byte array that corresponds to this instance of the object
	 */
	public byte[] toBytes() {
		return SerializationUtils.serialize(this);
	}
	
	/**
	 * Deserializes the specified bytes to an object who's super class is UDPBlob
	 * @param bytes The bytes of an object that extend UDPBlob
	 * @return The UDPBlob object version of the bytes specified
	 */
	@SuppressWarnings("unchecked")
	public static <T extends UDPBlob> T fromBytes(byte[] bytes) {
		return (T)SerializationUtils.deserialize(bytes);
	}

	public InetAddress getAddress() {
		return fromAddress;
	}

	public void setFromAddress(InetAddress fromAddress) {
		this.fromAddress = fromAddress;
	}

	public int getPort() {
		return fromPort;
	}

	public void setFromPort(int fromPort) {
		this.fromPort = fromPort;
	}
	
	
}
