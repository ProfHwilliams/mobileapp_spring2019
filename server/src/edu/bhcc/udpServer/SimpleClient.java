package edu.bhcc.udpServer;

import java.net.InetAddress;
import java.util.Scanner;

public class SimpleClient extends AbstractUDPArbiter<UDPBlob>{
	
	public static void main(String[] args) throws Exception {
		new SimpleClient();
	}

	public SimpleClient() throws Exception {
		super(InetAddress.getLocalHost(), 1234);
		
		Scanner keyboard = new Scanner(System.in);
		
		boolean isRun = true;
		
		while(isRun) {
			
			String line = keyboard.nextLine();
			
			UDPBlob message;
			
			if(line.toLowerCase().equals("end")) {
				message = new TerminateCommand();
				isRun = false;
			}
			else {
				message = new StringMessage(line);
			}
			
			send(message);			
		}
		
		keyboard.close();
		terminate();
	}


	@Override
	public void receive(UDPBlob blob) {
		System.out.println("FROM SERVER "+blob);
	}

}
