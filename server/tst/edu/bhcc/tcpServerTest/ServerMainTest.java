package edu.bhcc.tcpServerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.omg.CORBA.PRIVATE_MEMBER;

import com.google.gson.Gson;

import edu.bhcc.httpServer.HTTPServer;
import edu.bhcc.httpServer.ServerDelegator;
import rawhttp.core.RawHttp;
import rawhttp.core.RawHttpRequest;
import rawhttp.core.RawHttpResponse;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

public class ServerMainTest {

	@Test
	public void testGet() throws Exception {
		
		int port = 1234;
		
		HTTPServer server = new HTTPServer(port, new ServerDelegator() {
			
			@Override
			public String put(List<NameValuePair> params, String body) {
				return "BINGO";
			}
			
			@Override
			public String post(List<NameValuePair> params, String body) {
				return "BINGO";
			}
			
			@Override
			public String get(List<NameValuePair> params, String body) {
				return "BINGO";
			}
			
			@Override
			public String delete(List<NameValuePair> params, String body) {
				return "BINGO";
			}
			
			@Override
			public String connect(List<NameValuePair> params, String body) {
				return "BINGO";
			}
		});
		
		Thread.sleep(150L);
		
		RawHttp http = new RawHttp();

        RawHttpRequest request = http.parseRequest("GET /\r\nHost: localhost");

        Socket socket = new Socket(InetAddress.getLoopbackAddress(), port);
        request.writeTo(socket.getOutputStream());
        
        socket.setSoTimeout(15*1000);

        // get the response
        RawHttpResponse<?> response = http.parseResponse(socket.getInputStream()).eagerly();
        System.out.println("RESPONSE:\n" + response);
        assertEquals(200, response.getStatusCode());
        assertTrue(response.getBody().isPresent());
        assertEquals("BINGO", response.getBody().get().decodeBodyToString(StandardCharsets.UTF_8));
	}


}
