package edu.bhcc.udpServerTest;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class EchoClient {
    private static DatagramSocket socket;
    private static InetAddress address;
    private static byte[] buf;
 
    public static void main(String[] args) throws SocketException, UnknownHostException {
        socket = new DatagramSocket();
        address = InetAddress.getByName("192.168.1.212");
        
        while(true) {
        	try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        	
        	PointerInfo a = MouseInfo.getPointerInfo();
        	Point b = a.getLocation();
        	
        	try {
				sendEcho(b.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
        	
        }
        
    }
 
    public static String sendEcho(String msg) throws IOException {
        buf = msg.getBytes();
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 61113);
        socket.send(packet);
        packet = new DatagramPacket(buf, buf.length);
        socket.receive(packet);
        String received = new String(packet.getData(), 0, packet.getLength());
        return received;
    }
 
    public void close() {
        socket.close();
    }
}